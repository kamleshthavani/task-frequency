import React from "react";
import { CardHeader, CardTitle, Container, Row, Card, Col, Button} from "reactstrap";


class TaskFrequency extends React.Component {


    state = {
        periodicals: [
            "Bathroom floor",
            "Full window clean",
            "Lounge chairs",
            "Dining room Chairs",
            "High level Cleaning",
            "Vacant room clean",
            "Carpet steam cleaning",
            "Carpet fans cleaning",
            "Floor scrubbing",
            "Courtyard"
          ],
        frequencies: [
            {name: "Yearly", items:[]},
            {name: "Half Yearly", items:[]},
            {name: "3 TIMES A YEAR", items:[]},
            {name: "Quarterly", items:[]},
            {name: "6 TIMES A YEAR", items:[]},
            {name: "AS REQUIRED", items:[]},
            {name: "Hallways on rotation", items:[]},
            {name: "NA", items:[]}  
        ],
        styles : {marginTop: "20px", marginLeft:"20px", borderColor:"grey"}
    }

    onDragStart = (ev, id) => {
        ev.dataTransfer.setData("id", id);
    }

    onDragOver = (ev) => {
        ev.preventDefault();
    }

    onDrop = (ev, cat) => {
        let id = ev.dataTransfer.getData("id");

        let periodicals = this.state.periodicals.filter((item) => item !== id);
        
        let frequencies = this.state.frequencies.filter((frequency) => {
            if (frequency.name == cat) {
               frequency.items.push(id);
            }
            return frequency;
        });

        this.setState({
           ...this.state,
           periodicals,
           frequencies
       });
    }

    render() {
        var periodicals = [];
        var frequencies = [];
       
        this.state.periodicals.forEach ((x) => {
            periodicals.push(
                <Col key={x} sm={"6"} xs={"12"} md={"2"} lg={"2"} className="mt-1">
                  <Button 
                    outline 
                    color="warning"
                    onDragStart = {(e) => this.onDragStart(e, x)}
                    draggable
                  >{x}</Button>{' '}
                </Col>
            );
        });

        this.state.frequencies.forEach ((t) => {
            frequencies.push(
                <Col key={t.name} sm={"6"} xs={"12"} md={"3"} lg={"3"} className="mt-1"  onDragOver={(e)=>this.onDragOver(e)} onDrop={(e)=>{this.onDrop(e,t.name)}}>
                    <Card style={{"minHeight":"300px", "boxShadow": "none", "overflow": "unset", "position": "unset"}}>
                        <CardHeader  className="bg-primary text-white text-uppercase text-center">{t.name}</CardHeader>
                        {
                            t.items.map(x =>                                
                                <Col key={t.name} sm={"12"} xs={"12"} md={"12"} lg={"12"} className="mt-1">
                                     <Button 
                                        outline 
                                        color="warning"
                                        draggable
                                    >{x}</Button>{' '}
                                </Col>
                            )
                        }
                    </Card>
                </Col>
            );
        });

        return (
            <Container fluid>
                {/* Periodical Section starts here */}
                <Row style={this.state.styles}>
                    <Card 
                        body
                        onDragOver={(e)=>this.onDragOver(e)}
                        onDrop={(e)=>{this.onDrop(e, "periodicals")}}
                    >
                        <CardTitle> <strong>PERIODICALS</strong></CardTitle>
                        <Row >
                            {periodicals}
                        </Row>
                    </Card>
                </Row>
                {/* Periodical Section ends here */}

                 {/* Frequency Section starts here */}
                 <Row style={this.state.styles}>
                    <Card 
                        body  
                    >
                        <CardTitle> <strong>FREQUENCIES</strong></CardTitle>
                        <Row>
                            {frequencies}
                        </Row>
                    </Card>
                </Row>
                {/* Frequency Section ends here */}

            </Container>
        );
    }
}

export default TaskFrequency;